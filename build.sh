mkdir -p build \
    && cd build \
    && cmake `clsdk-cmake-args` .. \
    && make -j $(nproc) && \
    cltester -v tests.wasm && \
    cltester -v tests-reward.wasm && \
    cltester -v tests-farming.wasm && \
    cltester -v tests-mint.wasm && \
    cd ..

cp build/stakingtoken.* ./stakingtoken/  || true
cp build/reward.* ./reward/ || true
cp build/farming.* ./farming/  || true
cp build/mint.* ./mint/ || true