#include <tester-base.hpp>

#define CATCH_CONFIG_RUNNER

TEST_CASE( "Basic Test" ) {
  tester t;

  t.fund_accounts();
}

TEST_CASE( "Basic Configuration" ) {
  tester t;

  uint32_t                          day_in_sec = 86400;
  uint64_t                          block_pay = 10000; // 1.0000 LIBRE
  std::vector< eosio::symbol_code > pools = { eosio::symbol_code( "PBTC" ),
                                              eosio::symbol_code( "PUSDT" ) };
  eosio::symbol_code                test_symbol( "TEST" );

  expect(
      t.alice.trace< reward::actions::init >( day_in_sec, block_pay, pools ),
      "Missing required authority" );
  expect( t.rewardlibre.trace< reward::actions::init >(
              day_in_sec,
              block_pay,
              std::vector< eosio::symbol_code >{} ),
          "Need to set allowed poools" );
  t.rewardlibre.act< reward::actions::init >( day_in_sec, block_pay, pools );

  auto global = t.get_globals();

  CHECK( global.distribution_interval_sec == day_in_sec );
  CHECK( global.reward_per_block == block_pay );
  CHECK( global.allowed_pools == pools );

  day_in_sec -= 43200;
  block_pay += 10000;

  expect( t.alice.trace< reward::actions::setinterval >( day_in_sec ),
          "Missing required authority" );
  expect( t.alice.trace< reward::actions::setblockrwd >( block_pay ),
          "Missing required authority" );
  expect( t.alice.trace< reward::actions::addpool >( test_symbol ),
          "Missing required authority" );

  t.rewardlibre.act< reward::actions::setinterval >( day_in_sec );
  t.rewardlibre.act< reward::actions::setblockrwd >( block_pay );
  t.rewardlibre.act< reward::actions::addpool >( test_symbol );

  pools.push_back( test_symbol );
  auto global2 = t.get_globals();

  CHECK( global2.distribution_interval_sec == day_in_sec );
  CHECK( global2.reward_per_block == block_pay );
  CHECK( global2.allowed_pools == pools );

  t.rewardlibre.act< reward::actions::rmpool >( eosio::symbol_code( "PBTC" ) );
  t.rewardlibre.act< reward::actions::rmpool >( eosio::symbol_code( "PUSDT" ) );

  expect( t.rewardlibre.trace< reward::actions::rmpool >(
              eosio::symbol_code( "TESTA" ) ),
          "Pool does not exist" );

  t.rewardlibre.act< reward::actions::rmpool >( test_symbol );

  auto global3 = t.get_globals();

  CHECK( global3.allowed_pools.empty() );
}

TEST_CASE( "Notify Stake and Withdraw" ) {
  tester t;

  t.fund_accounts();

  uint32_t                          day_in_sec = 86400;
  uint64_t                          block_pay = 10000; // 1.0000 LIBRE
  std::vector< eosio::symbol_code > pools = { eosio::symbol_code( "BTCLIB" ),
                                              eosio::symbol_code( "BTCUSD" ) };

  t.rewardlibre.act< reward::actions::init >( day_in_sec, block_pay, pools );
  t.alice.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "farm.libre"_n,
                                        s2a( "3.000000000 BTCLIB" ),
                                        "memo" );
  t.alice.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "farm.libre"_n,
                                        s2a( "1.000000000 BTCLIB" ),
                                        "memo" );
  t.alice.act< farming::actions::withdraw >( "alice"_n,
                                             s2a( "2.000000000 BTCLIB" ) );
  t.alice.act< farming::actions::withdraw >( "alice"_n,
                                             s2a( "1.000000000 BTCLIB" ) );

  std::map< eosio::name, std::vector< uint64_t > > expected{
      { "alice"_n,
        std::vector{ static_cast< uint64_t >( 1000000000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 0 ) } } };

  CHECK( t.get_providers( eosio::symbol_code( "BTCLIB" ) ) == expected );

  std::vector< eosio::asset > expected2{ s2a( "1.000000000 BTCLIB" ) };

  CHECK( t.get_stats( "reward.libre"_n.value ) == expected2 );
}

TEST_CASE( "Pool does not exist" ) {
  tester t;

  t.fund_accounts();

  uint32_t                          day_in_sec = 86400;
  uint64_t                          block_pay = 10000; // 1.0000 LIBRE
  std::vector< eosio::symbol_code > pools = { eosio::symbol_code( "BTCLIB" ) };

  t.rewardlibre.act< reward::actions::init >( day_in_sec, block_pay, pools );
  expect( t.alice.with_code( "swap.libre"_n )
              .trace< token::actions::transfer >( "alice"_n,
                                                  "farm.libre"_n,
                                                  s2a( "3.000000000 BTCUSD" ),
                                                  "memo" ),
          "Pool is not accepted" );
}

TEST_CASE( "Validate rewards" ) {
  tester t;

  t.skip_to( "2022-07-04T06:00:00.000" );
  t.fund_accounts();

  uint32_t                          day_in_sec = 86400;
  uint64_t                          block_pay = 10000; // 1.0000 LIBRE
  std::vector< eosio::symbol_code > pools = { eosio::symbol_code( "BTCLIB" ),
                                              eosio::symbol_code( "BTCUSD" ) };

  t.rewardlibre.act< reward::actions::init >( day_in_sec, block_pay, pools );

  t.alice.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000001000 BTCLIB" ),
                                        "memo" );
  t.bob.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000025000 BTCLIB" ),
                                        "memo" );
  t.pip.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "pip"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000012000 BTCLIB" ),
                                        "memo" );
  t.egeon.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "egeon"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000007500 BTCLIB" ),
                                        "memo" );
  t.bertie.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "bertie"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000000500 BTCLIB" ),
                                        "memo" );
  t.ahab.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "ahab"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000019800 BTCLIB" ),
                                        "memo" );

  t.skip_to( "2022-07-04T23:59:59.500" );

  expect( t.rewardlibre.trace< reward::actions::updateall >( 100 ),
          "Nothing to do" );

  t.skip_to( "2022-07-05T06:00:00.000" );

  expect( t.rewardlibre.trace< reward::actions::updateall >( 0 ),
          "Nothing to do" );

  t.rewardlibre.act< reward::actions::updateall >( 100 );

  expect( t.rewardlibre.trace< reward::actions::updateall >( 10 ),
          "Nothing to do" );

  std::map< eosio::name, std::vector< uint64_t > > expected{
      { "ahab"_n,
        std::vector{ static_cast< uint64_t >( 19800 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 259987841 ) } },
      { "alice"_n,
        std::vector{ static_cast< uint64_t >( 1000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 13130699 ) } },
      { "bertie"_n,
        std::vector{ static_cast< uint64_t >( 500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 6565349 ) } },
      { "bob"_n,
        std::vector{ static_cast< uint64_t >( 25000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 328267477 ) } },
      { "egeon"_n,
        std::vector{ static_cast< uint64_t >( 7500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 98480243 ) } },
      { "pip"_n,
        std::vector{ static_cast< uint64_t >( 12000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 157568389 ) } } };

  CHECK( t.get_providers( eosio::symbol_code( "BTCLIB" ) ) == expected );

  t.alice.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000001000 BTCUSD" ),
                                        "memo" );
  t.bob.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000025000 BTCUSD" ),
                                        "memo" );
  t.pip.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "pip"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000012000 BTCUSD" ),
                                        "memo" );
  t.egeon.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "egeon"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000007500 BTCUSD" ),
                                        "memo" );
  t.bertie.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "bertie"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000000500 BTCUSD" ),
                                        "memo" );
  t.ahab.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "ahab"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000019800 BTCUSD" ),
                                        "memo" );
  t.skip_to( "2022-07-06T06:00:00.000" );

  t.rewardlibre.act< reward::actions::updateall >( 200 );

  std::map< eosio::name, std::vector< uint64_t > > expected2{
      { "ahab"_n,
        std::vector{ static_cast< uint64_t >( 19800 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 259987841 ) } },
      { "alice"_n,
        std::vector{ static_cast< uint64_t >( 1000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 13130699 ) } },
      { "bertie"_n,
        std::vector{ static_cast< uint64_t >( 500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 6565349 ) } },
      { "bob"_n,
        std::vector{ static_cast< uint64_t >( 25000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 328267477 ) } },
      { "egeon"_n,
        std::vector{ static_cast< uint64_t >( 7500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 98480243 ) } },
      { "pip"_n,
        std::vector{ static_cast< uint64_t >( 12000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 157568389 ) } } };

  CHECK( t.get_providers( eosio::symbol_code( "BTCUSD" ) ) == expected2 );

  t.skip_to( "2022-07-07T06:00:00.000" );
  t.rewardlibre.act< reward::actions::updateall >( 200 );

  // 3 days after for BTCLIB
  std::map< eosio::name, std::vector< uint64_t > > expected3{
      { "ahab"_n,
        std::vector{ static_cast< uint64_t >( 19800 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 259987841 * 3 ) } },
      { "alice"_n,
        std::vector{ static_cast< uint64_t >( 1000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 13130699 * 3 ) } },
      { "bertie"_n,
        std::vector{ static_cast< uint64_t >( 500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 6565349 * 3 ) } },
      { "bob"_n,
        std::vector{ static_cast< uint64_t >( 25000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 328267477 * 3 ) } },
      { "egeon"_n,
        std::vector{ static_cast< uint64_t >( 7500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 98480243 * 3 ) } },
      { "pip"_n,
        std::vector{ static_cast< uint64_t >( 12000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 157568389 * 3 ) } } };

  // 2 days after for BTCUSD
  std::map< eosio::name, std::vector< uint64_t > > expected4{
      { "ahab"_n,
        std::vector{ static_cast< uint64_t >( 19800 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 259987841 * 2 ) } },
      { "alice"_n,
        std::vector{ static_cast< uint64_t >( 1000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 13130699 * 2 ) } },
      { "bertie"_n,
        std::vector{ static_cast< uint64_t >( 500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 6565349 * 2 ) } },
      { "bob"_n,
        std::vector{ static_cast< uint64_t >( 25000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 328267477 * 2 ) } },
      { "egeon"_n,
        std::vector{ static_cast< uint64_t >( 7500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 98480243 * 2 ) } },
      { "pip"_n,
        std::vector{ static_cast< uint64_t >( 12000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 157568389 * 2 ) } } };

  CHECK( t.get_providers( eosio::symbol_code( "BTCLIB" ) ) == expected3 );
  CHECK( t.get_providers( eosio::symbol_code( "BTCUSD" ) ) == expected4 );
}

TEST_CASE( "Distribution with no pools" ) {
  tester t;

  t.skip_to( "2022-07-04T06:00:00.000" );
  t.fund_accounts();

  uint32_t                          day_in_sec = 86400;
  uint64_t                          block_pay = 10000; // 1.0000 LIBRE
  std::vector< eosio::symbol_code > pools = { eosio::symbol_code( "BTCLIB" ),
                                              eosio::symbol_code( "BTCUSD" ) };

  t.rewardlibre.act< reward::actions::init >( day_in_sec, block_pay, pools );
  t.alice.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000001000 BTCLIB" ),
                                        "memo" );
  t.rewardlibre.act< reward::actions::rmpool >(
      eosio::symbol_code( "BTCLIB" ) );
  t.rewardlibre.act< reward::actions::rmpool >(
      eosio::symbol_code( "BTCUSD" ) );

  t.skip_to( "2022-07-05T06:00:00.000" );

  expect( t.rewardlibre.trace< reward::actions::updateall >( 100 ),
          "Nothing to do" );
}

TEST_CASE( "Remove pool in a distribution" ) {
  tester t;

  t.skip_to( "2022-07-04T06:00:00.000" );
  t.fund_accounts();

  uint32_t                          day_in_sec = 86400;
  uint64_t                          block_pay = 10000; // 1.0000 LIBRE
  std::vector< eosio::symbol_code > pools = { eosio::symbol_code( "BTCLIB" ),
                                              eosio::symbol_code( "BTCUSD" ) };

  t.rewardlibre.act< reward::actions::init >( day_in_sec, block_pay, pools );
  t.alice.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000001000 BTCLIB" ),
                                        "memo" );
  t.bob.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000001000 BTCLIB" ),
                                        "memo" );
  t.egeon.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "egeon"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000001000 BTCLIB" ),
                                        "memo" );
  t.rewardlibre.act< reward::actions::rmpool >(
      eosio::symbol_code( "BTCLIB" ) );

  t.skip_to( "2022-07-05T06:00:00.000" );
  t.rewardlibre.act< reward::actions::updateall >( 1 );

  expect( t.rewardlibre.trace< reward::actions::rmpool >(
              eosio::symbol_code( "BTCUSD" ) ),
          "Cannot remove pool during distribution" );
}

TEST_CASE( "Tokens from fake contract" ) {
  tester t;

  t.skip_to( "2022-07-04T06:00:00.000" );
  t.fund_accounts();

  uint32_t                          day_in_sec = 86400;
  uint64_t                          block_pay = 10000; // 1.0000 LIBRE
  std::vector< eosio::symbol_code > pools = { eosio::symbol_code( "BTCLIB" ),
                                              eosio::symbol_code( "BTCUSD" ) };

  t.rewardlibre.act< reward::actions::init >( day_in_sec, block_pay, pools );
  t.farmfake.with_code( "farm.fake"_n )
      .act< farming::actions::stake >( "alice"_n, s2a( "0.000001000 BTCLIB" ) );

  std::map< eosio::name, std::vector< uint64_t > > expected{};

  CHECK( t.get_providers( eosio::symbol_code( "BTCLIB" ) ) == expected );

  std::vector< eosio::asset > expected2{};

  CHECK( t.get_stats( "reward.libre"_n.value ) == expected2 );
}

TEST_CASE( "Smaller updateall steps with one pool" ) {
  tester t;

  t.skip_to( "2022-07-04T06:00:00.000" );
  t.fund_accounts();

  uint32_t                          day_in_sec = 86400;
  uint64_t                          block_pay = 10000; // 1.0000 LIBRE
  std::vector< eosio::symbol_code > pools = { eosio::symbol_code( "BTCLIB" ) };

  t.rewardlibre.act< reward::actions::init >( day_in_sec, block_pay, pools );

  t.alice.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000001000 BTCLIB" ),
                                        "memo" );
  t.bob.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000025000 BTCLIB" ),
                                        "memo" );
  t.pip.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "pip"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000012000 BTCLIB" ),
                                        "memo" );
  t.egeon.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "egeon"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000007500 BTCLIB" ),
                                        "memo" );
  t.bertie.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "bertie"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000000500 BTCLIB" ),
                                        "memo" );
  t.ahab.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "ahab"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000019800 BTCLIB" ),
                                        "memo" );

  t.skip_to( "2022-07-05T06:00:00.000" );

  t.rewardlibre.act< reward::actions::updateall >( 1 ); // setup_distribution
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >( 1 ); // ahab
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >( 1 ); // alice
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >( 1 ); // bertie
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >( 1 ); // bob
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >( 2 ); // egeon and pip
  t.chain.start_block();

  expect( t.rewardlibre.trace< reward::actions::updateall >( 1 ),
          "Nothing to do" );

  std::map< eosio::name, std::vector< uint64_t > > expected{
      { "ahab"_n,
        std::vector{ static_cast< uint64_t >( 19800 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 519975683 ) } },
      { "alice"_n,
        std::vector{ static_cast< uint64_t >( 1000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 26261398 ) } },
      { "bertie"_n,
        std::vector{ static_cast< uint64_t >( 500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 13130699 ) } },
      { "bob"_n,
        std::vector{ static_cast< uint64_t >( 25000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 656534954 ) } },
      { "egeon"_n,
        std::vector{ static_cast< uint64_t >( 7500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 196960486 ) } },
      { "pip"_n,
        std::vector{ static_cast< uint64_t >( 12000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 315136778 ) } } };

  CHECK( t.get_providers( eosio::symbol_code( "BTCLIB" ) ) == expected );
}

TEST_CASE( "Smaller updateall steps with two pools" ) {
  tester t;

  t.skip_to( "2022-07-04T06:00:00.000" );
  t.fund_accounts();

  uint32_t                          day_in_sec = 86400;
  uint64_t                          block_pay = 10000; // 1.0000 LIBRE
  std::vector< eosio::symbol_code > pools = { eosio::symbol_code( "BTCLIB" ),
                                              eosio::symbol_code( "BTCUSD" ) };

  t.rewardlibre.act< reward::actions::init >( day_in_sec, block_pay, pools );

  t.alice.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000001000 BTCLIB" ),
                                        "memo" );
  t.bob.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "bob"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000025000 BTCLIB" ),
                                        "memo" );
  t.pip.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "pip"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000012000 BTCLIB" ),
                                        "memo" );
  t.egeon.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "egeon"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000007500 BTCUSD" ),
                                        "memo" );
  t.bertie.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "bertie"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000000500 BTCUSD" ),
                                        "memo" );
  t.ahab.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "ahab"_n,
                                        "farm.libre"_n,
                                        s2a( "0.000019800 BTCUSD" ),
                                        "memo" );

  t.skip_to( "2022-07-05T06:00:00.000" );

  t.rewardlibre.act< reward::actions::updateall >( 1 ); // setup_distribution
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >( 1 ); // alice - BTCLIB
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >( 1 ); // bob - BTCLIB
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >(
      1 ); // pip - BTCLIB and change pool
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >(
      2 ); // ahab and bertie - BTCUSD
  t.chain.start_block();

  t.rewardlibre.act< reward::actions::updateall >( 1 ); // egeon - BTCUSD
  t.chain.start_block();

  expect( t.rewardlibre.trace< reward::actions::updateall >( 1 ),
          "Nothing to do" );

  t.skip_to( "2022-07-06T05:59:59.500" );

  expect( t.rewardlibre.trace< reward::actions::updateall >( 1 ),
          "Nothing to do" );

  t.chain.start_block();
  t.rewardlibre.act< reward::actions::updateall >( 1 );

  std::map< eosio::name, std::vector< uint64_t > > expected_btclib{

      { "alice"_n,
        std::vector{ static_cast< uint64_t >( 1000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 22736842 ) } },

      { "bob"_n,
        std::vector{ static_cast< uint64_t >( 25000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 568421052 ) } },
      { "pip"_n,
        std::vector{ static_cast< uint64_t >( 12000 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 272842105 ) } } };

  std::map< eosio::name, std::vector< uint64_t > > expected_btcusd{
      { "ahab"_n,
        std::vector{ static_cast< uint64_t >( 19800 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 615366906 ) } },
      { "bertie"_n,
        std::vector{ static_cast< uint64_t >( 500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 15539568 ) } },
      { "egeon"_n,
        std::vector{ static_cast< uint64_t >( 7500 ),
                     static_cast< uint64_t >( 0 ),
                     static_cast< uint64_t >( 233093525 ) } } };

  CHECK( t.get_providers( eosio::symbol_code( "BTCLIB" ) ) == expected_btclib );
  CHECK( t.get_providers( eosio::symbol_code( "BTCUSD" ) ) == expected_btcusd );
}