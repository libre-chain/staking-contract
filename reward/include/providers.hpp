#pragma once

#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>

#include <constants.hpp>
#include <utils.hpp>

namespace reward {
  struct provider_v0 {
    eosio::name           account;
    uint64_t              total_staked;
    uint64_t              claimed;
    uint64_t              unclaimed;
    eosio::time_point_sec last_claim;

    uint64_t primary_key() const { return account.value; }
  };
  EOSIO_REFLECT( provider_v0,
                 account,
                 total_staked,
                 claimed,
                 unclaimed,
                 last_claim )

  using provider_variant = std::variant< provider_v0 >;

  struct provider {
    provider_variant value;
    FORWARD_MEMBERS( value,
                     account,
                     total_staked,
                     claimed,
                     unclaimed,
                     last_claim )
    FORWARD_FUNCTIONS( value, primary_key )
  };
  EOSIO_REFLECT( provider, value )

  using provider_table_type = eosio::multi_index< "provider"_n, provider >;

  class providers {
  private:
    eosio::name         contract;
    eosio::symbol_code  scope;
    provider_table_type _provider;

  public:
    providers( eosio::name contract, eosio::symbol_code scope )
        : contract( contract ), scope( scope ),
          _provider( contract, scope.raw() ) {}

    const provider_table_type &get_table() const { return _provider; }
    void stake( eosio::name account, eosio::asset quantity );
    void on_rmprovider( eosio::name account );
    void on_claim( eosio::name account );
    void set_staked( eosio::name account, uint64_t amount );
    void sub_staked( eosio::name account, uint64_t amount );
    void add_balance( eosio::name account, uint64_t amount );
    void sub_balance( eosio::name account, uint64_t amount );
    bool is_valid_pool( eosio::symbol_code pool );
    void on_clearall();
  };

} // namespace  reward