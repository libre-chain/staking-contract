#include <tester-base.hpp>

#define CATCH_CONFIG_RUNNER

#ifndef ENABLE_TESTNET
TEST_CASE( "Basic permissions, validations and end date" ) {
  tester t;

  t.fund_accounts();
  t.configure_mintrush(); // end_date = 1673938800 = Tuesday, January 17, 2023 7:00:00 AM
  t.skip_to( "2023-01-07T07:00:00.000" );
  t.setup_contributions();

  expect( t.alice.trace< mint::actions::init >( s2a( "500.000000 BTCLIB" ) ),
          "Missing required authority" );
  expect(
      t.mintlibre.trace< mint::actions::init >( s2a( "500.000000 BTCLIB" ) ),
      "Mint rush has not finished yet" );

  t.skip_to( "2023-01-16T06:59:59.500" );

  expect(
      t.mintlibre.trace< mint::actions::init >( s2a( "500.000000 BTCLIB" ) ),
      "Mint rush has not finished yet" );

  t.skip_to( "2023-01-17T07:00:00.000" );

  expect( t.mintlibre.trace< mint::actions::init >( s2a( "0.999999 BTCLIB" ) ),
          "Invalid token amount" );
  expect( t.mintlibre.trace< mint::actions::init >( s2a( "500.000000 OTHER" ) ),
          "Invalid token symbol" );

  t.mintlibre.act< mint::actions::init >( s2a( "1.000000 BTCLIB" ) );

  CHECK( t.get_global().total_lp_tokens == s2a( "1.000000 BTCLIB" ) );
}

TEST_CASE( "Migrate accounts" ) {
  tester t;

  t.fund_accounts();
  t.configure_mintrush(); // end_date = 1673938800 = Tuesday, January 17, 2023 7:00:00 AM
  t.skip_to( "2023-01-07T07:00:00.000" );
  t.setup_contributions();
  t.skip_to( "2023-01-16T06:59:59.500" );

  expect( t.mintlibre.trace< mint::actions::migrate >( 100 ),
          "The Smart Contract is not initialized yet" );

  t.skip_to( "2023-01-17T07:00:00.000" );

  t.mintlibre.act< mint::actions::init >( s2a( "500.000000 BTCLIB" ) );
  t.alice.act< mint::actions::migrate >( 100 );

  t.chain.start_block();

  expect( t.alice.trace< mint::actions::migrate >( 100 ), "Migration is over" );

  // Accounts were migrated after slow mode
  std::map< eosio::name, eosio::asset > expected = {
      { "ahab"_n, s2a( "1.086761 BTCLIB" ) },
      { "alice"_n, s2a( "0.000543 BTCLIB" ) },
      { "bertie"_n, s2a( "0.054338 BTCLIB" ) },
      { "bob"_n, s2a( "0.010867 BTCLIB" ) },
      { "egeon"_n, s2a( "0.163014 BTCLIB" ) },
      { "pip"_n, s2a( "0.054338 BTCLIB" ) } };

  CHECK( t.get_daily_reward() == expected );
}

TEST_CASE( "Slow accounts migration" ) {
  tester t;

  t.fund_accounts();
  t.configure_mintrush(); // end_date = 1673938800 = Tuesday, January 17, 2023 7:00:00 AM
  t.skip_to( "2023-01-07T07:00:00.000" );
  t.setup_contributions();
  t.skip_to( "2023-01-16T06:59:59.500" );

  expect( t.mintlibre.trace< mint::actions::migrate >( 100 ),
          "The Smart Contract is not initialized yet" );

  t.skip_to( "2023-01-17T07:00:00.000" );

  t.mintlibre.act< mint::actions::init >( s2a( "500.000000 BTCLIB" ) );

  // fetch_account
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();

  // update_pct
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();
  t.alice.act< mint::actions::migrate >( 1 );
  t.chain.start_block();

  expect( t.alice.trace< mint::actions::migrate >( 100 ), "Migration is over" );

  // Accounts were migrated after slow mode
  std::map< eosio::name, eosio::asset > expected = {
      { "ahab"_n, s2a( "1.086761 BTCLIB" ) },
      { "alice"_n, s2a( "0.000543 BTCLIB" ) },
      { "bertie"_n, s2a( "0.054338 BTCLIB" ) },
      { "bob"_n, s2a( "0.010867 BTCLIB" ) },
      { "egeon"_n, s2a( "0.163014 BTCLIB" ) },
      { "pip"_n, s2a( "0.054338 BTCLIB" ) } };

  CHECK( t.get_daily_reward() == expected );
}

TEST_CASE( "Distribution validations" ) {
  tester t;

  t.fund_accounts();
  t.configure_mintrush(); // end_date = 1673938800 = Tuesday, January 17, 2023 7:00:00 AM
  t.skip_to( "2023-01-07T07:00:00.000" );
  t.setup_contributions();
  t.skip_to( "2023-01-17T07:00:00.000" );

  t.mintlibre.act< mint::actions::init >( s2a( "500.000000 BTCLIB" ) );

  expect( t.alice.trace< mint::actions::distribute >( 100 ),
          "singleton does not exist" );

  t.alice.act< mint::actions::migrate >( 100 );
  t.chain.start_block();

  expect( t.bob.trace< mint::actions::claim >( "alice"_n ),
          "Missing required authority" );
  expect( t.alice.trace< mint::actions::claim >( "alice"_n ),
          "No funds to claim" );

  t.skip_to( "2023-01-18T07:00:00.000" );
  t.distribute( 100 );

  CHECK( t.get_global().days_since_eomr == 365 );

  std::map< eosio::name, eosio::asset > expected_share = {
      { "ahab"_n, s2a( "1.086761 BTCLIB" ) },
      { "alice"_n, s2a( "0.000543 BTCLIB" ) },
      { "bertie"_n, s2a( "0.054338 BTCLIB" ) },
      { "bob"_n, s2a( "0.010867 BTCLIB" ) },
      { "egeon"_n, s2a( "0.163014 BTCLIB" ) },
      { "pip"_n, s2a( "0.054338 BTCLIB" ) } };

  expected_share["ahab"_n] *= 365;
  expected_share["alice"_n] *= 365;
  expected_share["bertie"_n] *= 365;
  expected_share["bob"_n] *= 365;
  expected_share["egeon"_n] *= 365;
  expected_share["pip"_n] *= 365;

  CHECK( t.get_unclaimed() == expected_share );
}

TEST_CASE( "Distribution period" ) {
  tester t;

  t.fund_accounts();
  t.configure_mintrush(); // end_date = 1673938800 = Tuesday, January 17, 2023 7:00:00 AM
  t.skip_to( "2023-01-07T07:00:00.000" );
  t.setup_contributions();
  t.skip_to( "2023-01-17T07:00:00.000" );

  t.mintlibre.act< mint::actions::init >( s2a( "500.000000 BTCLIB" ) );
  t.alice.act< mint::actions::migrate >( 100 );

  t.chain.start_block();

  // Validate daily rewards
  std::map< eosio::name, eosio::asset > expected = {
      { "ahab"_n, s2a( "1.086761 BTCLIB" ) },
      { "alice"_n, s2a( "0.000543 BTCLIB" ) },
      { "bertie"_n, s2a( "0.054338 BTCLIB" ) },
      { "bob"_n, s2a( "0.010867 BTCLIB" ) },
      { "egeon"_n, s2a( "0.163014 BTCLIB" ) },
      { "pip"_n, s2a( "0.054338 BTCLIB" ) } };

  CHECK( t.get_daily_reward() == expected );

  expect( t.alice.trace< mint::actions::distribute >( 100 ), "Nothing to do" );

  t.skip_to( "2023-01-18T06:59:59.500" );

  expect( t.alice.trace< mint::actions::distribute >( 100 ), "Nothing to do" );

  t.skip_to( "2023-01-18T07:00:00.000" );

  std::map< eosio::name, eosio::asset > expected_share = {
      { "ahab"_n, s2a( "0.000000 BTCLIB" ) },
      { "alice"_n, s2a( "0.000000 BTCLIB" ) },
      { "bertie"_n, s2a( "0.000000 BTCLIB" ) },
      { "bob"_n, s2a( "0.000000 BTCLIB" ) },
      { "egeon"_n, s2a( "0.000000 BTCLIB" ) },
      { "pip"_n, s2a( "0.000000 BTCLIB" ) } };

  CHECK( t.get_unclaimed() == expected_share );

  t.alice.act< mint::actions::distribute >( 100 );

  expected_share["ahab"_n] = s2a( "1.086761 BTCLIB" );
  expected_share["alice"_n] = s2a( "0.000543 BTCLIB" );
  expected_share["bertie"_n] = s2a( "0.054338 BTCLIB" );
  expected_share["bob"_n] = s2a( "0.010867 BTCLIB" );
  expected_share["egeon"_n] = s2a( "0.163014 BTCLIB" );
  expected_share["pip"_n] = s2a( "0.054338 BTCLIB" );

  CHECK( t.get_unclaimed() == expected_share );

  expect( t.alice.trace< mint::actions::distribute >( 1 ), "Nothing to do" );

  t.skip_to( "2023-01-19T07:00:00.000" );

  t.alice.act< mint::actions::distribute >( 1 ); // setup distribution
  t.chain.start_block();

  CHECK( t.get_unclaimed() == expected_share );

  t.alice.act< mint::actions::distribute >( 1 ); // ahab
  t.chain.start_block();
  expected_share["ahab"_n] *= 2;

  CHECK( t.get_unclaimed() == expected_share );

  t.alice.act< mint::actions::distribute >( 1 ); // alice
  t.chain.start_block();
  expected_share["alice"_n] *= 2;

  CHECK( t.get_unclaimed() == expected_share );

  t.alice.act< mint::actions::distribute >( 1 ); // bertie
  t.chain.start_block();
  expected_share["bertie"_n] *= 2;

  CHECK( t.get_unclaimed() == expected_share );

  t.alice.act< mint::actions::distribute >( 1 ); // bob
  t.chain.start_block();
  expected_share["bob"_n] *= 2;

  CHECK( t.get_unclaimed() == expected_share );

  t.alice.act< mint::actions::distribute >( 1 ); // egeon
  t.chain.start_block();
  expected_share["egeon"_n] *= 2;

  CHECK( t.get_unclaimed() == expected_share );

  t.alice.act< mint::actions::distribute >( 1 ); // pip
  t.chain.start_block();
  expected_share["pip"_n] *= 2;

  CHECK( t.get_unclaimed() == expected_share );

  expect( t.alice.trace< mint::actions::distribute >( 1 ), "Nothing to do" );
}

TEST_CASE( "Complete delay distribution" ) {
  tester t;

  t.fund_accounts();
  t.configure_mintrush(); // end_date = 1673938800 = Tuesday, January 17, 2023 7:00:00 AM
  t.skip_to( "2023-01-07T07:00:00.000" );
  t.setup_contributions();
  t.skip_to( "2023-01-25T07:00:00.000" );

  t.mintlibre.act< mint::actions::init >( s2a( "500.000000 BTCLIB" ) );
  t.alice.act< mint::actions::migrate >( 100 );

  t.alice.act< mint::actions::distribute >(
      100 ); // Tuesday, January 18, 2023 7:00:00 AM
  t.chain.start_block();
  t.alice.act< mint::actions::distribute >(
      100 ); // Tuesday, January 19, 2023 7:00:00 AM
  t.chain.start_block();
  t.alice.act< mint::actions::distribute >(
      100 ); // Tuesday, January 20, 2023 7:00:00 AM
  t.chain.start_block();
  t.alice.act< mint::actions::distribute >(
      100 ); // Tuesday, January 21, 2023 7:00:00 AM
  t.chain.start_block();
  t.alice.act< mint::actions::distribute >(
      100 ); // Tuesday, January 22, 2023 7:00:00 AM
  t.chain.start_block();
  t.alice.act< mint::actions::distribute >(
      100 ); // Tuesday, January 23, 2023 7:00:00 AM
  t.chain.start_block();
  t.alice.act< mint::actions::distribute >(
      100 ); // Tuesday, January 24, 2023 7:00:00 AM
  t.chain.start_block();
  t.alice.act< mint::actions::distribute >(
      100 ); // Tuesday, January 25, 2023 7:00:00 AM
  t.chain.start_block();

  expect( t.alice.trace< mint::actions::distribute >( 100 ), "Nothing to do" );
}

#endif