#include <accounts.hpp>
#include <distributions.hpp>
#include <globals.hpp>
#include <stakingtoken.hpp>

namespace mint {
  void distributions::enable_distribution() {
    eosio::check( !distribution_sing.exists(),
                  "Distribution is already initialized" );

    libre::mintstats_singleton stats_sing( staking_contract, staking_contract.value );
    auto               mint_stats = stats_sing.get();

    distribution_sing.get_or_create(
        contract,
        next_distribution{ .distribution_time =
                               mint_stats.end_date + eosio::days( 1 ) } );
  }

  bool distributions::setup_distribution() {
    auto dist = distribution_sing.get();

    if ( auto *next = std::get_if< next_distribution >( &dist ) ) {
      if ( next->distribution_time <= eosio::current_block_time() ) {
        distribution_sing.set( current_distribution{ { *next } }, contract );

        return true;
      }
    }

    return false;
  }

  bool distributions::has_distribution_finished() {
    return globals( contract ).get_days_since_eomr() == total_minting_days;
  }

  uint32_t distributions::on_distribute( uint32_t max_steps ) {
    eosio::check( !has_distribution_finished(), "Distribution has finished" );

    if ( max_steps && setup_distribution() ) {
      --max_steps;
    }

    auto dist = distribution_sing.get();

    if ( auto *curr = std::get_if< current_distribution >( &dist ) ) {
      accounts accounts( contract );
      auto    &account_tb = accounts.get_table();
      auto     itr = curr->last_processed != eosio::name{}
                         ? account_tb.find( curr->last_processed.value )
                         : account_tb.begin();

      for ( ; itr != account_tb.end() && max_steps > 0; ++itr, --max_steps ) {
        accounts.add_balance( itr->owner(), itr->daily_share() );
      }

      if ( itr != account_tb.end() ) {
        curr->last_processed = itr->owner();
        distribution_sing.set( *curr, contract );
      } else {
        globals( contract ).increase_day();
        distribution_sing.set(
            next_distribution{ .distribution_time =
                                   curr->distribution_time.to_time_point() +
                                   eosio::days( 1 ) },
            contract );
      }
    }

    return max_steps;
  }
} // namespace mint