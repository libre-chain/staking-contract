/*
 *
 * @author  EOSCostaRica.io [ https://eoscostarica.io ]
 *
 * @section DESCRIPTION
 *  Header file for the declaration of all functions
 *
 *  GitHub: https://github.com/edenia/staking-contract
 *
 */
#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>
#include <eosio/singleton.hpp>

#include <accounts.hpp>
#include <distributions.hpp>
#include <globals.hpp>

// import accounts/accounts

namespace mint {
  class minting : public eosio::contract {
  public:
    using contract::contract;

    minting( eosio::name                       receiver,
             eosio::name                       code,
             eosio::datastream< const char * > ds )
        : contract( receiver, code, ds ) {}

    void init( eosio::asset total_lp_tokens );
    void distribute( uint32_t max_steps );
    void claim( eosio::name account );

    void migrate( uint32_t max_steps );
  };

  EOSIO_ACTIONS( minting,
                 "mint.libre"_n,
                 action( init, total_lp_tokens ),
                 action( distribute, max_steps ),
                 action( claim, account ),
                 action( migrate, max_steps ) )

} // namespace mint