#!/usr/bin/env sh

set -e
docker build -t contract-builder:latest -f docker/builder.Dockerfile .
docker run --name contract-builder contract-builder:latest
docker cp contract-builder:/build .
docker rm contract-builder

pwd
cp build/stakingtoken.* ./stakingtoken/  || true
cp build/reward.* ./reward/ || true
cp build/farming.* ./farming/  || true
cp build/mint.* ./mint/  || true
