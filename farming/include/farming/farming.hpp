/*
 *
 * @author  EOSCostaRica.io [ https://eoscostarica.io ]
 *
 * @section DESCRIPTION
 *  Header file for the declaration of all functions
 *
 *  GitHub: https://github.com/edenia/staking-contract
 *
 */
#include <eosio/asset.hpp>
#include <eosio/eosio.hpp>

#include <constants.hpp>

namespace farming {
  struct account {
    eosio::name  account;
    eosio::asset balance;
    eosio::name  contract;

    uint64_t primary_key() const { return account.value; };
  };
  EOSIO_REFLECT( account, account, balance, contract )

  using account_table_type = eosio::multi_index< "account"_n, account >;

  uint64_t get_balance( eosio::name account, eosio::symbol_code symbol_code ) {
    account_table_type account_tb( "farm.libre"_n, symbol_code.raw() );

    auto itr = account_tb.find( account.value );

    return itr != account_tb.end() ? itr->balance.amount : 0;
  }

  class farming : public eosio::contract {
  private:
    const eosio::name REWARD_CONTRACT = "reward.libre"_n;

  public:
    using contract::contract;

    void notify_transfer( eosio::name  from,
                          eosio::name  to,
                          eosio::asset quantity,
                          std::string &memo );
    void stake( eosio::name account, eosio::asset quantity );
    void withdraw( eosio::name account, eosio::asset quantity );
  };

  EOSIO_ACTIONS( farming,
                 "farm.libre"_n,
                 action( stake, account, quantity ),
                 action( withdraw, account, quantity ),
                 notify( "swap.libre"_n, transfer ) )

} // namespace farming