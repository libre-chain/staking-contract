#include <tester-base.hpp>

#define CATCH_CONFIG_RUNNER

TEST_CASE( "Basic Transfer" ) {
  tester t;

  t.fund_accounts();

  t.alice.with_code( "swap.libre"_n )
      .act< token::actions::transfer >( "alice"_n,
                                        "farm.libre"_n,
                                        s2a( "3.000000000 BTCLIB" ),
                                        "memo" );

  std::map< eosio::name, eosio::asset > expected{
      { "alice"_n, s2a( "3.000000000 BTCLIB" ) } };

  CHECK( t.get_balance() == expected );

  t.alice.act< farming::actions::withdraw >( "alice"_n,
                                             s2a( "1.400000000 BTCLIB" ) );

  std::map< eosio::name, eosio::asset > expected2{
      { "alice"_n, s2a( "1.600000000 BTCLIB" ) } };

  CHECK( t.get_balance() == expected2 );

  expect( t.alice.trace< farming::actions::withdraw >(
              "alice"_n,
              s2a( "1.600000001 BTCLIB" ) ),
          "Overdrawn balance" );

  t.alice.act< farming::actions::withdraw >( "alice"_n,
                                             s2a( "1.600000000 BTCLIB" ) );

  std::map< eosio::name, eosio::asset > expected3{
      { "alice"_n, s2a( "0.000000000 BTCLIB" ) } };

  CHECK( t.get_balance() == expected3 );

  // WORKING: failure tests
}
